
const GameViewReducer = (viewSize = { width: 0, height: 0 }, action: {type: string, payload: any}) => {
    switch (action.type) {
        case 'UPDATE_GAMEVIEW':
            let newViewSize = { width: 0, height: 0 };
            newViewSize.width = action.payload.width;
            newViewSize.height = action.payload.height;
            return newViewSize
        default:
            return viewSize
    }
};

export default GameViewReducer