
const ScaleFactorReducer = (scaleFactor = 1, action: {type: string, payload: any}) => {
    switch (action.type) {
        case 'UPDATE_SCALEFACTOR':
            const newScaleFactor = Math.round(((action.payload.width / 1000 + action.payload.height / 1000) / 2) * 1000 ) / 1000;
            return newScaleFactor
        default:
            return scaleFactor
    }
};

export default ScaleFactorReducer