
export const updateScaleFactor = (height: number, width: number) => {
    return {
        type: 'UPDATE_SCALEFACTOR',
        payload: {
            width: width,
            height: height
        }
    };
};

export default updateScaleFactor