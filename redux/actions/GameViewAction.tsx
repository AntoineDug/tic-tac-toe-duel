
export const UpdateGameView = (layout: any) => {
    return {
        type: 'UPDATE_GAMEVIEW',
        payload: {
            width: layout.width,
            height: layout.height
        }
    };
};

export default UpdateGameView