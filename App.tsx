import React from 'react';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { StatusBar } from 'expo-status-bar';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

import MainScene from './pages/MainScene';
import ScaleFactorReducer from './redux/reducers/ScaleFactorReducer';
import GameViewReducer from './redux/reducers/GameViewReducer';

const store = createStore (
  combineReducers({
    scaleFactor: ScaleFactorReducer,
    GameView: GameViewReducer
  })
);

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#b8ffab',
  },
};

export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <MainScene/>
        <StatusBar/>
      </PaperProvider>
    </Provider>
  );
}