import React from 'react';
import { connect } from 'react-redux';
import { DataTable } from 'react-native-paper';
import { Text } from 'react-native';

interface props {
    scaleFactor?: number,
    historyData: {
        key: number,
        player1: string,
        player2: string,
        winner: string,
        sessionScore: string
    }[]
}

const HistoryTable = (props: props) => {
    const scale = props.scaleFactor ? props.scaleFactor : 0
    const [currentPage, setCurrentPage] = React.useState(0);
    let itemsPerPage = Math.round(10 * (scale/1.2));
    let from = currentPage * itemsPerPage;
    let to = (currentPage + 1) * itemsPerPage;
    let fontSizeHeader = 20 * (Math.sqrt(scale));
    let fontSizeRow = 16 * (Math.sqrt(scale));

    return (
        <DataTable style={ {width: 600 * (Math.sqrt(scale)), height: 550 * scale}}>
            <DataTable.Header>
                <DataTable.Title><Text style={{fontSize: fontSizeHeader}}>player1</Text></DataTable.Title>
                <DataTable.Title><Text style={{fontSize: fontSizeHeader}}>player2</Text></DataTable.Title>
                <DataTable.Title><Text style={{fontSize: fontSizeHeader}}>winner</Text></DataTable.Title>
                <DataTable.Title><Text style={{fontSize: fontSizeHeader}}>sessionScore</Text></DataTable.Title>
            </DataTable.Header>

            {
                props.historyData.slice(from, to).map((item: any) => { return (
                    <DataTable.Row key={item.key}>
                        <DataTable.Cell><Text style={{fontSize: fontSizeRow}}>{item.player1}</Text></DataTable.Cell>
                        <DataTable.Cell><Text style={{fontSize: fontSizeRow}}>{item.player2}</Text></DataTable.Cell>
                        <DataTable.Cell><Text style={{fontSize: fontSizeRow}}>{item.winner}</Text></DataTable.Cell>
                        <DataTable.Cell><Text style={{fontSize: fontSizeRow}}>{item.sessionScore}</Text></DataTable.Cell>
                    </DataTable.Row>
                )})
            }
            
            <DataTable.Pagination page={currentPage} numberOfPages={Math.ceil(props.historyData.length / itemsPerPage)} onPageChange={page => setCurrentPage(page)}/>
        </DataTable>
    );
}

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(HistoryTable)