import React from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet, TextInput } from 'react-native';

interface props {
    name: string,
    color: string
    scaleFactor?: number
}

const Player = (props: props) => {
    const scale = props.scaleFactor ? props.scaleFactor : 0
    return (
        <View>
            <PlayerScore scale={scale}/>
            <TextInput style={[style.playerInput, {borderColor: props.color, fontSize: 25 * scale, borderRadius: 9 * scale}]} value={props.name} />
        </View>
    );
}

interface props2 {
    scale: number
}

const PlayerScore = (props: props2) => {
    return (
        <Text style={[style.scoreText, {fontSize: 20 * props.scale}]}>Total win: </Text>
    );
}

const style = StyleSheet.create({
    playerInput: {
        color: 'white',
        borderWidth: 4,
        borderColor: 'black',
        textAlign: 'center',
        margin: 5,
        minWidth: 120
    },
    scoreText: {
        color: 'white'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(Player)