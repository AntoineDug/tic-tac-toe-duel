import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, TouchableOpacity, Animated, Easing } from 'react-native';
import { Actions } from 'react-native-router-flux';

interface props {
    title: string
    sceneKey: any
    scaleFactor?: number
}

const MenuButton = (props: props) => {
    const scale = props.scaleFactor ? props.scaleFactor : 0
    
    const buttonAnimations = {
        top: 0,
        left: 0,
        rotateY: 0,
        rotateX: 0,
        skewX: 0,
        skewY: 0
    }

    switch (props.sceneKey) {
        case "history":
            buttonAnimations.top = -250 * scale;
            buttonAnimations.left = -400 * scale;
            buttonAnimations.rotateY = 0.2;
            buttonAnimations.rotateX = 0;
            buttonAnimations.skewX = -0.10;
            buttonAnimations.skewY = -0.06;
            break;
        case "game":
            buttonAnimations.top = -500 * scale;
            buttonAnimations.left = 0;
            buttonAnimations.rotateY = 0;
            buttonAnimations.rotateX = 0.2;
            buttonAnimations.skewX = 0;
            buttonAnimations.skewY = 0;
            break;
        case "setting":
            buttonAnimations.top = -250 * scale;
            buttonAnimations.left = 400 * scale;
            buttonAnimations.rotateY = -0.2;
            buttonAnimations.rotateX = 0;
            buttonAnimations.skewX = 0.10;
            buttonAnimations.skewY = 0.06;
            break;
        default:
            break;
    }

    const topAnim = useRef(new Animated.Value(0)).current;
    const leftAnim = useRef(new Animated.Value(0)).current;
    const rotateYAnim = useRef(new Animated.Value(0)).current;
    const rotateXAnim = useRef(new Animated.Value(0)).current;
    const skewXAnim = useRef(new Animated.Value(0)).current;
    const skewYAnim = useRef(new Animated.Value(0)).current;

    const rotateY = rotateYAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const rotateX = rotateXAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const skewX = skewXAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const skewY = skewYAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    const buttonOut = () => {
        Animated.parallel([
            Animated.timing(topAnim, {
                toValue: buttonAnimations.top,
                duration: 700,
                easing: Easing.circle,
                useNativeDriver: false
            }),
            Animated.timing(leftAnim, {
                toValue: buttonAnimations.left,
                duration: 700,
                easing: Easing.circle,
                useNativeDriver: false
            }),
            Animated.timing(rotateYAnim, {
                toValue: buttonAnimations.rotateY,
                duration: 700,
                easing: Easing.linear,
                useNativeDriver: false
            }),
            Animated.timing(rotateXAnim, {
                toValue: buttonAnimations.rotateX,
                duration: 700,
                easing: Easing.linear,
                useNativeDriver: false
            }),
            Animated.timing(skewXAnim, {
                toValue: buttonAnimations.skewX,
                duration: 700,
                easing: Easing.exp,
                useNativeDriver: false
            }),
            Animated.timing(skewYAnim, {
                toValue: buttonAnimations.skewY,
                duration: 700,
                easing: Easing.ease,
                useNativeDriver: false
            })
        ]).start(() => Actions.replace(props.sceneKey));
    };

    return (
        <TouchableOpacity onPress={buttonOut} style={{alignSelf: 'center'}}>
            <Animated.View key="animed" style={[style.button, {width: 200 * scale, height: 200 * scale, transform: [{translateX: leftAnim}, {translateY: topAnim}, { rotateY: rotateY }, { rotateX: rotateX }, { skewX: skewX }, { skewY: skewY }]}]}>
                <Text style={[style.buttonText, {fontSize: 30 * scale}]}>{props.title}</Text>
            </Animated.View>
        </TouchableOpacity>
    );
}

const style = StyleSheet.create({
    button: {
        borderColor: 'white',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        position: 'relative',
        transform: [{
            perspective: 200
        }]
    },
    buttonText: {
        color: 'white'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(MenuButton)