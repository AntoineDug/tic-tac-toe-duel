import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StyleSheet, View, Dimensions } from 'react-native';

interface props {
    scaleFactor?: number
}

const GameGrid = (props: props) => {
    let {height} = Dimensions.get("window");
    height *= (5/6);
    const scale = props.scaleFactor ? props.scaleFactor : 0
    const gridSize = 500 * scale;
    const thickness = 6 * scale;
    return (
        <View style={{ position: 'absolute', top: (height - gridSize) / 2, left: -gridSize/2, height: gridSize }}>
            <Grid style={[style.container, { width: gridSize }]}>
                <Row style={style.container}>
                    <Col style={[style.container, {borderBottomWidth: thickness, borderRightWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderLeftWidth: thickness, borderRightWidth: thickness, borderBottomWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderLeftWidth: thickness, borderBottomWidth: thickness}]}></Col>
                </Row>
                <Row style={style.container}>
                    <Col style={[style.container, {borderBottomWidth: thickness, borderTopWidth: thickness, borderRightWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderBottomWidth: thickness, borderTopWidth: thickness, borderLeftWidth: thickness}]}></Col>
                </Row>
                <Row style={style.container}>
                    <Col style={[style.container, {borderRightWidth: thickness, borderTopWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderLeftWidth: thickness, borderRightWidth: thickness, borderTopWidth: thickness}]}></Col>
                    <Col style={[style.container, {borderTopWidth: thickness, borderLeftWidth: thickness}]}></Col>
                </Row>
            </Grid>
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(GameGrid)