import React from "react";
import { Dimensions } from "react-native";
import Animated from "react-native-reanimated";
import Svg, { ClipPath, Defs, Path, Rect } from "react-native-svg";

const svgX = 'M 30.9 22.5 L 43.4 10 C 44.9 8.5 44.9 6 43.4 4.5 L 40.6 1.7 C 39 0.1 36.5 0.1 35 1.7 L 22.5 14.2 L 10 1.6 C 8.5 0.1 6 0.1 4.5 1.6 L 1.6 4.4 C 0.1 6 0.1 8.4 1.6 10 L 14.2 22.5 L 1.6 35 C 0.1 36.5 0.1 39 1.6 40.5 L 4.4 43.3 C 6 44.9 8.4 44.9 10 43.3 L 22.5 30.9 L 35 43.4 C 36.5 44.9 39 44.9 40.5 43.4 L 43.3 40.6 C 44.9 39 44.9 36.5 43.3 35 L 30.9 22.5 Z'

export const O = (props: any) => {
  const {width} = Dimensions.get("window");

  const diameter = props.size * 1.2;
  const x = props.body.position.x - diameter / 2;
  const y = props.body.position.y - diameter / 2;

  return (
    <Animated.View
      style={{
        position: "absolute",
        left: x - width/2,
        top: y,
        width: diameter,
        height: diameter,
        borderRadius: 50,
        borderWidth: diameter * 0.2,
        borderColor: "blue"
    }}/>
  );
};

export const X = (props: any) => {
  const {width} = Dimensions.get("window");

  const size = props.size * 1.25;
  const x = props.body.position.x - size / 2;
  const y = props.body.position.y - size / 2;
  const angle = props.body.angle;

  return (
    <Svg style={{
      position: "absolute",
      left: x - width/2 + size * (1/20),
      top: y + size * (1/20),
      width: size,
      height: size,
      transform: [{ rotate: angle + "rad" }]
    }}>
      <Defs>
        <ClipPath id="clip">
          <Path d={svgX}/>
        </ClipPath>
      </Defs>
      <Rect width={size} height={size} scale={1.4 * (size / 70)} fill="red" clipPath="url(#clip)" />
    </Svg>
  );
};

export default O;
