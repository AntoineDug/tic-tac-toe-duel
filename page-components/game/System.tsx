import Matter from "matter-js";
import { O, X } from './Render';
import { Audio } from 'expo-av';

const pieceSound = new Audio.Sound();
pieceSound.loadAsync(require('../../assets/piece-placement.mp3'));

export const Physics = (state: any, { time }: any) => {
	let engine = state["physics"].engine;

	Matter.Engine.update(engine, time.delta);

	return state;
};

export const GameState = (state: any, { screen }: any) => {
    const init = state["initial"];

    if (init.create) {
        const bodySize = init.bodySize;
        const boundsThickness = 400;
		let world = state["physics"].world;
		const boxWidth = screen.width;
		const boxHeight = screen.height * (5 / 6);
    	const bodyXvectors = Matter.Vertices.fromPath('30.9 22.5 43.4 10 44.4 7.2 43.4 4.5 40.6 1.7 37.8 0.7 35 1.7 22.5 14.2 10 1.6 7.2 0.6 4.5 1.6 1.6 4.4 0.6 7.2 1.6 10 14.2 22.5 1.6 35 0.6 37.8 1.6 40.5 4.4 43.3 7.2 44.3 10 43.3 22.5 30.9 35 43.4 37.8 44.4 40.5 43.4 43.3 40.6 44.3 37.8 43.3 35');
		
		const xBoundTop = Matter.Bodies.rectangle(boxWidth/2, -boundsThickness/2 + 10, boxWidth*2, boundsThickness, { isStatic: true, density: 10 });
        const xBoundButt = Matter.Bodies.rectangle(boxWidth/2, boxHeight + boundsThickness/2 - 10, boxWidth*2, boundsThickness, { isStatic: true, density: 10 });
        const yBoundLeft = Matter.Bodies.rectangle(-boundsThickness/2 + 10, boxHeight/2, boundsThickness, boxHeight*2, { isStatic: true, density: 10 });
        const yBoundRight = Matter.Bodies.rectangle(boxWidth + boundsThickness/2 - 10, boxHeight/2, boundsThickness, boxHeight*2, { isStatic: true, density: 10 });
        Matter.World.add(world, [xBoundTop, xBoundButt, yBoundLeft, yBoundRight]);

        for (let i = 1; i <= init.piece; i++) {
            const bodyO = Matter.Bodies.circle(boxWidth / 6, boxHeight / 2 * (i / (init.piece/2) * 0.8), bodySize / 2, { frictionAir: 0.1 });
			const bodyX = Matter.Bodies.fromVertices(boxWidth * (5/6), boxHeight / 2 * (i / (init.piece/2) * 0.8), [bodyXvectors], { frictionAir: 0.1 });
			Matter.Body.scale(bodyO, 1.2, 1.2);
			Matter.Body.scale(bodyX, 1.4 * (bodySize / 70), 1.4 * (bodySize / 70));

            Matter.World.add(world, [bodyO, bodyX]);

            state['O' + i] = { body: bodyO, size: bodySize, renderer: O};
            state['X' + i] = { body: bodyX, size: bodySize, renderer: X};
        }
        state["initial"].create = false;
    }

	return state;
};

export const DragPiece = (state: any, { touches }: any) => {

    let constraint = state["physics"].constraint;

	let start = touches.find((x:any) => x.type === "start");
	let move = touches.find((x:any) => x.type === "move");
    let end = touches.find((x:any) => x.type === "end");

	if (start) {
		let startPos = [start.event.pageX, start.event.pageY];
		let piece = state.find((item:any) => item.body && Math.hypot((startPos[0] - item.body.position.x), (startPos[1] - item.body.position.y)) < 25)

		if (piece) {
			constraint.pointA = { x: startPos[0], y: startPos[1] };
			constraint.bodyB = state[piece].body;
			constraint.pointB = { x: 0, y: 0 };
			constraint.angleB = state[piece].body.angle;
		}
	}

	if (move) {
		constraint.pointA = { x: move.event.pageX, y: move.event.pageY };
	}

	if (end) {
		pieceSound.playAsync();
		constraint.pointA = null;
		constraint.bodyB = null;
		constraint.pointB = null;
	}

    return state;
}

export default GameState