import React from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Setting = (props: any) => {
    return (
        <View style={style.container}>
            <Text style={{color: 'white'}}>SETTING</Text>
            <Text onPress={() => Actions.replace("menu")} style={{color: 'white'}}>Back</Text>
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(Setting)