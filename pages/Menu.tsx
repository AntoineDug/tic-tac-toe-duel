import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet, Image, Animated, Easing } from 'react-native';

import MenuButton from '../page-components/menu/MenuButton';
import Player from '../page-components/menu/Player';


const Menu = (props: any) => {

    const scale = props.scaleFactor ? props.scaleFactor : 0
    const sizeAnim = useRef(new Animated.Value(1)).current;
    const rotaionYAnim = useRef(new Animated.Value(0)).current;

    const imageBounce = Animated.loop(
        Animated.sequence([
            Animated.timing(sizeAnim, {
                toValue: 0.8,
                duration: 1200,
                useNativeDriver: true,
                easing: Easing.ease
            }),
            Animated.timing(sizeAnim, {
                toValue: 1,
                duration: 1200,
                useNativeDriver: true,
                easing: Easing.ease
            })
        ])
    );

    const imageflip = Animated.loop(
        Animated.sequence([
            Animated.timing(rotaionYAnim, {
                toValue: 1,
                duration: 1200,
                delay: 5000,
                useNativeDriver: true,
                easing: Easing.cubic
            }),
            Animated.timing(rotaionYAnim, {
                toValue: 0,
                duration: 1000,
                delay: 3500,
                useNativeDriver: true,
                easing: Easing.cubic
            })
        ])
    );

    const imageAnimation = Animated.parallel([
        imageBounce,
        imageflip
    ]);

    const rotaionY = rotaionYAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })

    imageAnimation.start();

    return (
        <View style={style.container}>
            <View style={style.titleContainer}>
                <View style={style.titleContainer}>
                    <Animated.Image style={{width: 100 * scale, height: 100 * scale, transform: [{rotateY: rotaionY}, {scaleX: sizeAnim}, {scaleY: sizeAnim}]}} source={require('../assets/Tic_tac_toe_logo.png')} />
                </View>
                <View style={style.titleContainer}>
                <   Text style={[style.title, {fontSize: 50 * scale}]}>Tic Tac Toe Duel</Text>
                </View>
                <View style={style.titleContainer}></View>
            </View>
            <View style={[style.buttonContainer, {minHeight: 230 * scale}]}>
                <MenuButton title="history" sceneKey="history"/>
                <MenuButton title="game" sceneKey="game"/>
                <MenuButton title="setting" sceneKey="setting"/>
            </View>
            <View style={style.playerContainer}>
                <Player name="player1" color="blue"/>
                <Player name="player2" color="red"/>
            </View>
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%'
    },
    titleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 2,
        flexDirection: 'row',
        marginHorizontal: '5%',
        justifyContent: 'space-around'
    },
    playerContainer: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: '10%',
        justifyContent: 'space-around'
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        width: 300,
        textAlign: 'center',
        fontFamily: 'sans-serif-light'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(Menu)