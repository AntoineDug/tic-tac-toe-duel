import React, { useEffect, useRef } from 'react';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { connect, useSelector } from 'react-redux';
import { Text, View, StyleSheet, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { GameEngine } from "react-native-game-engine";
import Matter from 'matter-js';
import { Audio } from 'expo-av';

import UpdateGameView from '../redux/actions/GameViewAction';
import GameGrid from '../page-components/game/GameGrid';
import { Physics, DragPiece, GameState } from '../page-components/game/System';
import decomp from 'poly-decomp';
window.decomp = decomp;

Matter.Common.isElement = () => false;

const Game = (props: any) => {
    const gameRef = useRef(null);
    const viewSize = useSelector(state => state.GameView);
    const scale = useSelector(state => state.scaleFactor);
    const bodySize = 70 * scale;

    const engine = Matter.Engine.create({ enableSleeping: false });
    const world = engine.world;
    world.gravity.scale = 0;
    
    const constraint = Matter.Constraint.create({
        label: "Drag Constraint",
        pointA: { x: 0, y: 0 },
        pointB: { x: 0, y: 0 },
        length: 0.01
    });
    Matter.World.addConstraint(world, constraint);

    useEffect(() => {
        if (Platform.OS === 'web') {
            const pieceSound = new Audio.Sound();
            pieceSound.loadAsync(require('../assets/piece-placement.mp3'));
            const mouse = Matter.Mouse.create(gameRef.current);
            const mouseConstraint = Matter.MouseConstraint.create(engine, { mouse: mouse });
            Matter.Events.on(mouseConstraint, "enddrag", () => {
		        pieceSound.playAsync();
            })
            Matter.World.add(world, mouseConstraint);
        }
    });

    return (
        <View style={[style.container, {flex: 1}]}>
            <View style={[style.container, {flex: 5}]} ref={gameRef} onLayout={(event) => {props.UpdateGameView(event.nativeEvent.layout);}}>
                <GameEngine
                systems={[Physics, DragPiece, GameState]}
                entities={{
                    physics: { engine: engine, world: world, constraint: constraint },
                    initial: { piece: 5, bodySize: bodySize, create: true},
                    grid: { renderer: GameGrid }
                }}/>
            </View>
            <View style={[style.container, {flex: 1}]}>
                <Text onPress={() => Actions.replace("menu")} style={{fontSize: 20, color: 'white'}}>Back</Text>
            </View>
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => (
    bindActionCreators({
        UpdateGameView
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Game)