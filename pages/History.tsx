import React from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

import HistoryTable from '../page-components/history/HistoryTable'

let historyData = [
    {
        key: 0,
        player1: 'player11',
        player2: 'player21',
        winner: 'winner1',
        sessionScore: 'sessionScore1'
    },
    {
        key: 1,
        player1: 'player12',
        player2: 'player22',
        winner: 'winner2',
        sessionScore: 'sessionScore2'
    },
    {
        key: 2,
        player1: 'player13',
        player2: 'player23',
        winner: 'winner3',
        sessionScore: 'sessionScore3'
    }
]

const Menu = (props: any) => {
    const scale = props.scaleFactor ? props.scaleFactor : 0
    return (
        <View style={style.container}>
            <HistoryTable historyData={historyData}/>
            <TouchableOpacity onPress={() => Actions.replace("menu")}>
                <View style={{width: 150 * scale, height: 150 * scale}}>
                    <FontAwesomeIcon icon={faTimes} style={[style.buttonContent, {color: 'blue'}]}/>
                    <Text style={[style.buttonContent, {fontSize: 20 * scale, color: 'white', textAlign: 'center', textAlignVertical: 'center'}]}>Back</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    buttonContent: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = (state: any) => {
    return {
      scaleFactor: state.scaleFactor
    }
}

export default connect(mapStateToProps, null)(Menu)