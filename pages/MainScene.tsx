import React, { useEffect } from 'react';
import { Dimensions, ImageBackground, StyleSheet, View } from 'react-native';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Router, Scene } from 'react-native-router-flux';

import updateScaleFactor from '../redux/actions/ScaleFactorAction'
import Menu from './Menu';
import History from './History';
import Game from './Game';
import Setting from './Setting';

const MainScene = (props: any) => {
  const {height, width} = Dimensions.get('window')
  props.updateScaleFactor(height, width);
  let buffer: number;
  
  useEffect(() => {
    const handleResize = (e: {window: {height: number, width: number}}) => {
      props.updateScaleFactor(e.window.height, e.window.width);
    }

    clearTimeout(buffer);
    buffer = setTimeout(() => {
      Dimensions.addEventListener('change', handleResize)
    }, 16)

    return () => {
      Dimensions.removeEventListener('change', handleResize)
    }
  })

  return (
    <View style={styles.container}>
      <ImageBackground source={require('../assets/background.jpg')} style={styles.backgroundImage}>
        <Router sceneStyle={{backgroundColor: "#00000000"}}>
          <Scene key="root" navTransparent hideNavBar>
            <Scene key="menu" component={Menu} initial/>
            <Scene key="history" component={History}/>
            <Scene key="game" component={Game}/>
            <Scene key="setting" component={Setting}/>
          </Scene>
        </Router>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  },
  backgroundImage: {
    width: '100%',
    height: '100%'
  }
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => (
  bindActionCreators({
    updateScaleFactor
  }, dispatch)
);

export default connect(null, mapDispatchToProps)(MainScene);